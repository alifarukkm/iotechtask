﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Net;
using System.Net.Sockets;
using System.Threading;

namespace Server
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private static byte[] _buffer = new byte[1024];
        private static Socket _serverSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
        private static List<Socket> _clientSockets = new List<Socket>();


        string text = "";
        string lampStatus = "0";
        public MainWindow()
        {    
            InitializeComponent();
            setupServer();
        }
        
        // Change Lamp and send client.
        private void btnLamp_Click(object sender, RoutedEventArgs e)
        {
            if(lampStatus == "0")
            {
                imgLamp.Source = new BitmapImage(new Uri(@"/lamp_closed.png", UriKind.Relative));
                btnLamp.Content = "Close";

                
                byte[] data = Encoding.ASCII.GetBytes(lampStatus);
                foreach (Socket soc in _clientSockets)
                {
                    soc.Send(data);
                }
                lampStatus = "1";

            }
            else
            {
                imgLamp.Source = new BitmapImage(new Uri(@"/lamp_opened.png", UriKind.Relative));
                btnLamp.Content = "Open";

                lampStatus = "1";
                byte[] data = Encoding.ASCII.GetBytes(lampStatus);
                foreach (Socket soc in _clientSockets)
                {

                    soc.Send(data);
                }

                lampStatus = "0";
            }
        }

        
        // Create random number and send client on txtBtn
        private void btnText_Click(object sender, RoutedEventArgs e)
        {
            Random rnd = new Random();
            int rndNumber = rnd.Next(1, 10000);
            txtBoxArea.Text = rndNumber.ToString();
        }
        
        // Change text box area on fly and send client on txtBtn
        private void TxtBoxArea_TextChanged(object sender, TextChangedEventArgs e)
        {   
            string text = txtBoxArea.Text;
            byte[] data = Encoding.ASCII.GetBytes(text);
            foreach (Socket soc in _clientSockets)
            {
                soc.Send(data);
            }
        }
        

        private void setupServer()
        {
            try
            {
                _serverSocket.Bind(new IPEndPoint(IPAddress.Any, 3000));
                _serverSocket.Listen(3);
                _serverSocket.BeginAccept(new AsyncCallback(AcceptCallback), null);
                txtStatus.Dispatcher.Invoke(() =>
                {
                    txtStatus.Text = "Server is listenning.";
                    txtStatus.Foreground = Brushes.DarkSeaGreen;
                });
               
            }
            catch
            {
                txtStatus.Dispatcher.Invoke(() =>
                {
                    txtStatus.Text = "Something Wrong!";
                    txtStatus.Foreground = Brushes.RosyBrown;
                });
                
            }
        }

        private void AcceptCallback(IAsyncResult AR)
        {
            Socket socket = _serverSocket.EndAccept(AR);
            _clientSockets.Add(socket);
            _buffer.DefaultIfEmpty();

            socket.BeginReceive(_buffer, 0, _buffer.Length, SocketFlags.None, new AsyncCallback(ReceiveCallback), socket);
            _serverSocket.BeginAccept(new AsyncCallback(AcceptCallback), null);

        }

        private void ReceiveCallback(IAsyncResult AR)
        {
            Socket socket = (Socket)AR.AsyncState;
            int received = socket.EndReceive(AR);
            byte[] dataBuf = new byte[received];
            Array.Copy(_buffer, dataBuf, received);

            text = Encoding.ASCII.GetString(dataBuf);
            
            if(text == "0")
            {
                imgLamp.Dispatcher.Invoke(() =>
                {
                    imgLamp.Source = new BitmapImage(new Uri(@"/lamp_closed.png", UriKind.Relative));
                    btnLamp.Content = "Close";
                    lampStatus = "0";
                    byte[] data = Encoding.ASCII.GetBytes(lampStatus);
                    foreach (Socket cl in _clientSockets)
                    {
                        cl.Send(data);
                    }
                    
                });
                
                
            }
            else if(text == "1")
            {
                imgLamp.Dispatcher.Invoke(() =>
                {
                    imgLamp.Source = new BitmapImage(new Uri(@"/lamp_opened.png", UriKind.Relative));
                    btnLamp.Content = "Open";
                    lampStatus = "1";
                    byte[] data = Encoding.ASCII.GetBytes(lampStatus);
                    foreach (Socket cl in _clientSockets)
                    {
                        cl.Send(data);
                    }
                });
                
            }
            else if(text == "rnd")
            {
                txtBoxArea.Dispatcher.Invoke(() =>
                {
                    Random rnd = new Random();
                    int rndNumber = rnd.Next(1, 10000);
                    txtBoxArea.Text = rndNumber.ToString();
                });
               
            }
            else
            {
                sendText(lampStatus, ref socket);
            }
            _buffer.DefaultIfEmpty();
            socket.BeginReceive(_buffer, 0, _buffer.Length, SocketFlags.None, new AsyncCallback(ReceiveCallback), socket);

        }

        private void sendText(string Text, ref Socket socket)
        {
            byte[] data = Encoding.ASCII.GetBytes(Text);
            socket.BeginSend(data, 0, data.Length, SocketFlags.None, new AsyncCallback(SendCallBack), null);
        }
        private void SendCallBack(IAsyncResult AR)
        {
            Socket socket = (Socket)AR.AsyncState;
            socket.EndSend(AR);

        }


    }
}
